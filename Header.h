#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkVertex.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkWorldPointPicker.h>
#include <vtkCamera.h>
#include <vtkSphereSource.h>
#include <vtkSTLReader.h>
#include <vector>
#include <iostream>
#include <cstdlib>
using namespace std;

struct Point3D
{
public:
	double x, y, z;
	Point3D() {}
	Point3D(double pos[3]) { this->x = pos[0]; this->y = pos[1]; this->z = pos[2]; }
	~Point3D() {}
};
typedef Point3D Vector3D;

Vector3D GetNormal(vector<Point3D>);

// Handle mouse events
class MouseInteractorStyle2 : public vtkInteractorStyleTrackballCamera
{
public:
	static MouseInteractorStyle2* New();
	vtkTypeMacro(MouseInteractorStyle2, vtkInteractorStyleTrackballCamera);

	virtual void OnRightButtonDown();

	virtual void OnMiddleButtonDown();

private:
	vector<Point3D> plane_points;
};
