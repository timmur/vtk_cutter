#include "Header.h"

void MouseInteractorStyle2::OnRightButtonDown()
{
	double clickPos[3] = { this->Interactor->GetSize()[0] / 2,this->Interactor->GetSize()[1] / 2, 0 };

	// Pick from this location.
	vtkSmartPointer<vtkWorldPointPicker>  picker =
		vtkSmartPointer<vtkWorldPointPicker>::New();
	picker->Pick(clickPos, this->GetDefaultRenderer());

	double* pos = picker->GetPickPosition();

	if (plane_points.size() < 3)
	{
		plane_points.push_back(Point3D(pos));
		system("CLS");
		cout << "current points: " << endl;
		for (int i = 0; i < plane_points.size(); i++)
		{
			cout << plane_points[i].x << " " << plane_points[i].y << " " << plane_points[i].z << endl;
		}
	}
	else
	{
		system("CLS");
		reverse(plane_points.begin(), plane_points.end());
		plane_points.pop_back();
		reverse(plane_points.begin(), plane_points.end());
		vtkActorCollection* actorCollection = this->GetDefaultRenderer()->GetActors();
		actorCollection->InitTraversal();
		vtkActor* nextActor = actorCollection->GetNextActor(); nextActor = actorCollection->GetNextActor();
		this->GetDefaultRenderer()->RemoveActor(nextActor);
		plane_points.push_back(Point3D(pos));
		cout << "current points: " << endl;
		for (int i = 0; i < plane_points.size(); i++)
		{
			cout << plane_points[i].x << " " << plane_points[i].y << " " << plane_points[i].z << endl;
		}
	}

	//draw a selected point
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();
	points->InsertNextPoint(pos);

	vtkSmartPointer<vtkVertex> vertex =
		vtkSmartPointer<vtkVertex>::New();
	vertex->GetPointIds()->SetId(0, 0);

	vtkSmartPointer<vtkCellArray> vertices =
		vtkSmartPointer<vtkCellArray>::New();
	vertices->InsertNextCell(vertex);

	vtkSmartPointer<vtkPolyData> polydata =
		vtkSmartPointer<vtkPolyData>::New();
	polydata->SetPoints(points);
	polydata->SetVerts(vertices);

	// Setup actor and mapper
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(polydata);

	vtkSmartPointer<vtkActor> actor =
		vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	actor->GetProperty()->SetPointSize(10);
	actor->GetProperty()->SetColor(255, 0, 0);

	this->GetDefaultRenderer()->AddActor(actor);
}


void MouseInteractorStyle2::OnMiddleButtonDown()
{
	if (plane_points.size() != 3) return;

	vtkActorCollection* _actorCollection = this->GetDefaultRenderer()->GetActors();
	_actorCollection->InitTraversal();
	double key[3] = { 1.2, 3.4, 5.6 };
	for (int i = 0; i < _actorCollection->GetNumberOfItems(); i++)
	{
		vtkActor* nextActor = _actorCollection->GetNextActor();
		if (nextActor->GetProperty()->GetColor()[0] == key[0] && nextActor->GetProperty()->GetColor()[1] == key[1])
		{
			this->GetDefaultRenderer()->RemoveActor(nextActor);
			break;
		}
	}

	system("CLS");
	cout << "cutting now" << endl;
	cout << "points: " << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << plane_points[i].x << " " << plane_points[i].y << " " << plane_points[i].z << endl;
	}
	// Create a plane to cut,here it cuts in the XZ direction (xz normal=(1,0,0);XY =(0,0,1),YZ =(0,1,0)
	vtkSmartPointer<vtkPlane> plane =
		vtkSmartPointer<vtkPlane>::New();
	plane->SetOrigin(plane_points[0].x, plane_points[0].y, plane_points[0].z);
	Vector3D n = GetNormal(plane_points);
	plane->SetNormal(n.x, n.y, n.z);
	cout << "normal vector: " << n.x << " " << n.y << " " << n.z << endl;


	// Create cutter
	vtkSmartPointer<vtkCutter> cutter =
		vtkSmartPointer<vtkCutter>::New();
	cutter->SetCutFunction(plane);

	// set cutter on the first actor on the scene aka 3d model
	// no way to get this simpler than that
	vtkActorCollection* actorCollection = this->GetDefaultRenderer()->GetActors();
	actorCollection->InitTraversal();
	vtkActor* nextActor = actorCollection->GetNextActor();

	cutter->SetInputConnection(nextActor->GetMapper()->GetInputConnection(0, 0));
	cutter->Update();

	vtkSmartPointer<vtkPolyDataMapper> cutterMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	cutterMapper->SetInputConnection(cutter->GetOutputPort());

	// Create plane actor
	vtkSmartPointer<vtkActor> planeActor =
		vtkSmartPointer<vtkActor>::New();
	planeActor->GetProperty()->SetColor(key);
	planeActor->GetProperty()->SetLineWidth(5);
	planeActor->SetMapper(cutterMapper);

	this->GetDefaultRenderer()->AddActor(planeActor);
}
