#include "Header.h"

vtkStandardNewMacro(MouseInteractorStyle2);

int main(int argc, char* argv[])
{
	std::string inputFilename;
	if (argc > 1)
		inputFilename = string(argv[1]);
	else { cout << "Please insert full path to your stl file as parameter" << endl; return EXIT_FAILURE;}
	vtkSmartPointer<vtkSTLReader> stlSource =
		vtkSmartPointer<vtkSTLReader>::New();
	stlSource->SetFileName(inputFilename.c_str());
	stlSource->Update();

	vtkPolyData* polydata = stlSource->GetOutput();
	vtkSmartPointer<vtkPolyDataMapper> stlMapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	stlMapper->SetInputData(polydata);

	// Create stl actor
	vtkSmartPointer<vtkActor> stlActor =
		vtkSmartPointer<vtkActor>::New();
	stlActor->GetProperty()->SetColor(0.5, 1, 0.5);
	stlActor->SetMapper(stlMapper);

	// Create renderer and add actor
	vtkSmartPointer<vtkRenderer> renderer =
		vtkSmartPointer<vtkRenderer>::New();
	renderer->AddActor(stlActor); 
	renderer->SetBackground(0, 0, 0);

    // Add renderer to renderwindow and render
	vtkSmartPointer<vtkRenderWindow> renderWindow =
		vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->AddRenderer(renderer);
	renderWindow->SetSize(600, 600);

	vtkSmartPointer<MouseInteractorStyle2> style =
		vtkSmartPointer<MouseInteractorStyle2>::New();
	style->SetDefaultRenderer(renderer);

	vtkSmartPointer<vtkRenderWindowInteractor> interactor =
		vtkSmartPointer<vtkRenderWindowInteractor>::New();
	interactor->SetRenderWindow(renderWindow);
	interactor->SetInteractorStyle(style);
	renderWindow->Render();

	interactor->Initialize();
	interactor->Start();

	return EXIT_SUCCESS;
}