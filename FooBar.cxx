#include "Header.h"

Vector3D GetNormal(vector<Point3D> ps)
{
	if (ps.size() != 3) return nullptr;
	Vector3D N;		double a[3];	double b[3];

	a[0] = ps[1].x - ps[0].x; a[1] = ps[1].y - ps[0].y; a[2] = ps[1].z - ps[0].z;
	b[0] = ps[2].x - ps[0].x; b[1] = ps[2].y - ps[0].y; b[2] = ps[2].z - ps[0].z;

	Vector3D A = Vector3D(a);	Vector3D B = Vector3D(b);

	N.x = (A.y*B.z) - (A.z*B.y);
	N.y = (A.z*B.x) - (A.x*B.z);
	N.z = (A.x*B.y) - (A.y*B.x);

	double mod = sqrt(N.x*N.x + N.y*N.y + N.z*N.z);
	N.x /= mod; N.y /= mod; N.z /= mod;
	return N;
}
